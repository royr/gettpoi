//
//  PlacesProvider.swift
//  gett poi
//
//  Created by Amit Cohen on 9/25/15.
//  Copyright © 2015 AmCo. All rights reserved.
//

import Foundation
import CoreLocation

class PlacesProvider  {
    static let sharedProvider = PlacesProvider()
    var placesArray = [Place]()
    
    let APIKey = "AIzaSyDOj7_mUuUhUHX_Cy9Syd-q1qPTn0Yg5ls"
    
    func placesInRadius(radius: Int, location: CLLocationCoordinate2D, types:[String:Bool]){
        var chosenTypes = [String]()
        for (str,bl) in types{
            if bl{
                chosenTypes.append(str)
            }
        }
        let typesStr = types.count > 0 ? "&types="+(chosenTypes.joinWithSeparator("%7C")) : ""
        let stringURL: String = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=\(location.latitude),\(location.longitude)&radius=\(radius)\(typesStr)&key=\(self.APIKey)"
        self.requestPlacesWithURLString(stringURL)
    }
    
    func placesInBounds(location: CLLocationCoordinate2D, southWest: CLLocationCoordinate2D, northEast: CLLocationCoordinate2D){
        let stringURL: String = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?latlng=\(location.latitude),\(location.longitude)&bounds=\(southWest.latitude),\(southWest.longitude)%7C\(northEast.latitude),\(northEast.longitude)&name=cruise&key=\(self.APIKey)"
        self.requestPlacesWithURLString(stringURL)
    }
    
    private func requestPlacesWithURLString(stringURL: String){
        if let url: NSURL? = NSURL(string: stringURL){
            let task = NSURLSession.sharedSession().dataTaskWithURL(url!, completionHandler:
                {
                    data, response, err in
                    if err != nil {
                        print("error\(err?.description)")
                    }else{
                        let jsonObj = JSON(data: data!)
                        let statusCode = jsonObj["status"].stringValue
                        switch statusCode {
                        case "OK":
                            var tempPlacesArray = [Place]()
                            var typesArray = [String]()
                            for (_, subJson):(String, JSON) in jsonObj["results"] {
                                typesArray.removeAll()
                                for (_,typeItem):(String, JSON) in subJson["types"] {
                                    typesArray.append(typeItem.stringValue)
                                }
                                let location = CLLocationCoordinate2DMake(subJson["geometry"]["location"]["lat"].double!, subJson["geometry"]["location"]["lng"].double!)
                                let tempPlace: Place = Place(name: subJson["name"].stringValue, address: subJson["vicinity"].stringValue, location: location, ID:subJson["id"].stringValue, types: typesArray)
                                tempPlacesArray.append(tempPlace)
                            }
                            self.placesArray = tempPlacesArray
                            NSNotificationCenter.defaultCenter().postNotificationName("placesUpdateNotification", object: nil)
                            break
                        case "ZERO_RESULTS":
                            print("ZERO_RESULTS")
                            break
                        case "OVER_QUERY_LIMIT":
                            print("OVER_QUERY_LIMIT")
                            break
                        case "REQUEST_DENIED":
                            print("REQUEST_DENIED")
                            break
                        case "INVALID_REQUEST":
                            print("INVALID_REQUEST")
                            break
                        case "UNKNOWN_ERROR":
                            print("UNKNOWN_ERROR")
                            break
                        default :
                            print("issue")
                            break
                        }
                    }
            })
            task.resume()
        }else{
            print("problem with URL")
        }
    }
}



