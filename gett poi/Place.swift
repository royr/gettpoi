//
//  Place.swift
//  gett poi
//
//  Created by Amit Cohen on 9/26/15.
//  Copyright © 2015 AmCo. All rights reserved.
//

import Foundation
import CoreLocation

struct Place {
    let name:String
    let address: String
    let location:CLLocationCoordinate2D
    let ID : String
    let types: Array<String>
}