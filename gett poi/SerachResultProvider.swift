//
//  SerachResultProvider.swift
//  gett poi
//
//  Created by Amit Cohen on 9/27/15.
//  Copyright © 2015 AmCo. All rights reserved.
//

import Foundation
import GoogleMaps

class SearchResultsProvider {
    var placesResultsArray = [Place]()
    static let sharedSearchResultsProvider = SearchResultsProvider()
    
    func searchAutocompleteWithText(text: String, complition:([String:String]->Void)) {
        if !text.isEmpty {
            let filter = GMSAutocompleteFilter()
            filter.type = GMSPlacesAutocompleteTypeFilter.NoFilter
            GMSPlacesClient.sharedClient().autocompleteQuery(text, bounds: nil, filter: filter, callback: { (results, error: NSError?) -> Void in
                if let error = error {
                    print("Autocomplete error \(error)")
                }
                var resultDic = [String:String]()
                for result in results! {
                    if let result = result as? GMSAutocompletePrediction {
                        resultDic[result.placeID] = result.attributedFullText.string
                    }
                }
                complition(resultDic) // Assuming internet connection is the buttole neck return the search results, later fill any missing info
                self.createPlaces(resultDic)
            })
        }
    }
    
    private func createPlaces(dic:[String:String]){
        self.placesResultsArray.removeAll()
        for (placeID,_) in dic {
            GMSPlacesClient.sharedClient().lookUpPlaceID(placeID, callback: { (place: GMSPlace?, error: NSError?) -> Void in
                if let error = error {
                    print("lookup place id query error: \(error.localizedDescription)")
                    return
                }
                if let place = place {
                    let newPlace = Place(name: place.name, address: place.formattedAddress, location: place.coordinate, ID: place.placeID, types: place.types as! [String])
                    self.placesResultsArray.append(newPlace)
                } else {
                    print("No place details for \(placeID)")
                }
            })
        }
    }
}