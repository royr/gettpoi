//
//  PlacesTableViewController.swift
//  gett poi
//
//  Created by Amit Cohen on 9/26/15.
//  Copyright © 2015 AmCo. All rights reserved.
//

import UIKit

class TypesTableViewController: UITableViewController {
    var typesArray = [String:Bool]()
    var arr = [String]()
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.reloadData()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    // MARK: - Table view data source
    func didSelectDeselectRow(tableView: UITableView, indexPath: NSIndexPath){
        let cell = tableView.cellForRowAtIndexPath(indexPath)
        let cellName = cell?.textLabel?.text
        typesArray[cellName!] = !(typesArray[cellName!]!)
        cell!.accessoryType = (cell!.accessoryType == .Checkmark) ? .None: .Checkmark
    }
    
    
     override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        didSelectDeselectRow(tableView, indexPath: indexPath)
    }
    override func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
        didSelectDeselectRow(tableView, indexPath: indexPath)
    }
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return typesArray.count
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("TypesCell", forIndexPath: indexPath)

        // Configure the cell...
        let stringsArray = [String](typesArray.keys)
        let selectedArray = [Bool](typesArray.values)
        cell.textLabel!.text = stringsArray[indexPath.row]
        cell.accessoryType = (selectedArray[indexPath.row]) ? .Checkmark : .None
        return cell
    }


       // MARK: - Navigation
    /*
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
