//
//  ViewController.swift
//  gett poi
//
//  Created by Amit Cohen on 9/25/15.
//  Copyright © 2015 AmCo. All rights reserved.
//

import UIKit
import CoreLocation
import GoogleMaps
import QuartzCore

class ViewController: UIViewController  {
    
    @IBOutlet weak var searchBarView: UISearchBar!
    @IBOutlet weak var resultsTableView: UITableView!
    @IBOutlet weak var mapView: GMSMapView!
    var updateMarkers = true
    var updateCenterMarker = true
    let centerMarker = GMSMarker()
    let placesProvider: PlacesProvider = PlacesProvider.sharedProvider
    let locationManager = CLLocationManager()
    var searchResults = [String:String]()
    var selectedRow: NSIndexPath = NSIndexPath.init(forRow: -1, inSection: -1)
    var typesArray = ["atm":false,"cafe":false,"bar":false,"bank":false,"restaurant":false,"lodging":false,"store":false]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        
        mapView.delegate = self
        centerMarker.appearAnimation = kGMSMarkerAnimationPop
        centerMarker.icon = GMSMarker.markerImageWithColor(UIColor.blueColor())
        resultsTableView.layer.cornerRadius = 10
        resultsTableView.layer.masksToBounds = true
        //just showing off here - KVO should be a better fit here, and in Swift probably callback or use of willset function of the property
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "updatePlacesMarkers", name: "placesUpdateNotification", object: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    
    @IBAction func unwindToMapView(unwindSegue: UIStoryboardSegue) {
        if let typesViewController = unwindSegue.sourceViewController as? TypesTableViewController {
            typesArray = typesViewController.typesArray
            placesProvider.placesInRadius(500, location:self.mapView.camera.target,types: typesArray)
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "typesSegue" {
            if let navVC = segue.destinationViewController as? UINavigationController{
                if let typesViewController = navVC.viewControllers.first as? TypesTableViewController {
                    typesViewController.typesArray = typesArray
                }
            }
        }
        
    }
    deinit{
        NSNotificationCenter.defaultCenter().removeObserver(self)
        
    }
    func updatePlacesMarkers(){
        let placesArray = placesProvider.placesArray
        dispatch_async(dispatch_get_main_queue()){
            self.mapView.clear()
            self.centerMarker.map = self.mapView
            for item in placesArray {
                let marker = GMSMarker()
                marker.position = item.location
                marker.appearAnimation = kGMSMarkerAnimationPop
                marker.title = item.name
                marker.snippet = item.address
                
                if item.types.contains("atm"){
                    marker.icon = UIImage(named: "ATM.png")
                }else if item.types.contains("bar"){
                    marker.icon = UIImage(named: "bar.png")
                }else if item.types.contains("bank"){
                    marker.icon = UIImage(named: "dolar.png")
                }else if item.types.contains("cafe") {
                    marker.icon = UIImage(named: "cafe.png")
                }else if item.types.contains("restaurant") || item.types.contains("food") {
                    marker.icon = UIImage(named: "restaurant.png")
                }else if item.types.contains("lodging") {
                    marker.icon = UIImage(named: "lodging.png")
                }else {
                    for placeType in item.types {
                        if placeType.containsString("store"){
                            marker.icon = UIImage(named: "store.png")
                            break
                        }
                    }
                }
                
                marker.map = self.mapView
            }
            
        }
    }
    
    func hideResultsView() {
        searchBarView.showsCancelButton = false
        resultsTableView.hidden = true
        searchBarView.resignFirstResponder()
    }
    
    
}
// MARK: - Table view data source
extension ViewController: UITableViewDataSource {
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return searchResults.count//placesProvider.placesArray.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("resultsCell", forIndexPath: indexPath)
        
        let placesNamesArray = [String](searchResults.values)
        cell.textLabel!.text = placesNamesArray[indexPath.row]
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.selectedRow = indexPath
        let placesIDArray = [String](searchResults.keys)
        let chosenId = placesIDArray[indexPath.row]
        for item in SearchResultsProvider.sharedSearchResultsProvider.placesResultsArray {
            if item.ID == chosenId {
                hideResultsView()
                self.mapView.camera = GMSCameraPosition(target: item.location, zoom: 15, bearing: 0, viewingAngle: 0)
                break
            }
        }
    }
}
//MARK: UISearchBarDelegate
extension ViewController: UISearchBarDelegate {
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        hideResultsView()
    }
    
    func searchBarResultsListButtonClicked(searchBar: UISearchBar) {
        
    }
    func searchBarTextDidBeginEditing(searchBar: UISearchBar) {
        if !searchResults.isEmpty {
            searchBar.showsCancelButton = true
            resultsTableView.hidden = false
        }
    }
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        //        placeAutocomplete(searchText)
        SearchResultsProvider.sharedSearchResultsProvider.searchAutocompleteWithText(searchText, complition: { places in
            self.searchResults = places
            self.resultsTableView.reloadData()
        })
        resultsTableView.reloadData()
        searchBar.showsCancelButton = true
        resultsTableView.hidden = false
    }
}

// MARK: GMSMapViewDelegate
extension ViewController : GMSMapViewDelegate {
    func mapView(mapView: GMSMapView!, willMove gesture: Bool) {
        if gesture {
            if selectedRow.row != -1 {
                self.resultsTableView.deselectRowAtIndexPath(selectedRow, animated: false)
            }
        }
    }
    
    func centerMarkerUpdate(position:CLLocationCoordinate2D){//could have been fixed positioned (like Gett app
        let handler = { (response : GMSReverseGeocodeResponse!, error: NSError!) -> Void in
            if error != nil {
                print("error\(error?.description)")
            }
            self.centerMarker.position = position
            if let result = response?.firstResult(){
                self.centerMarker.title = result.lines[0] as! String
                self.centerMarker.snippet = result.lines.count > 1 ? result.lines[1]  as? String: "Unknown region"
            }else {
                self.centerMarker.title = "unknown"
            }
            self.centerMarker.zIndex = 2
            self.centerMarker.map = self.mapView
        }
        GMSGeocoder().reverseGeocodeCoordinate(position, completionHandler: handler)
    }
    
    func mapView(mapView: GMSMapView!, idleAtCameraPosition cameraPosition: GMSCameraPosition!) {
        if updateCenterMarker {
            centerMarkerUpdate(cameraPosition.target)
        }else {
            updateCenterMarker = true
        }
        if updateMarkers {
            placesProvider.placesInRadius(500, location:cameraPosition.target,types: typesArray)
        }else{
            updateMarkers = true
        }
    }
    func mapView(mapView: GMSMapView!, didTapMarker marker: GMSMarker!) -> Bool {
        updateMarkers = false
        updateCenterMarker = false
        return false
    }
    
    func didTapMyLocationButtonForMapView(mapView: GMSMapView!) -> Bool {
        if selectedRow.row != -1 {
            self.resultsTableView.deselectRowAtIndexPath(selectedRow, animated: false)
        }
        return false;
    }
    func mapView(mapView: GMSMapView!, didTapAtCoordinate coordinate: CLLocationCoordinate2D) {
        hideResultsView()
    }
}

//MARK: CLLocationManagerDelegate
extension ViewController: CLLocationManagerDelegate {
    func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        if status == .AuthorizedWhenInUse {
            locationManager.startUpdatingLocation()
            mapView.myLocationEnabled = true
            mapView.settings.myLocationButton = true
        }
    }
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first{
            mapView.camera = GMSCameraPosition(target: location.coordinate, zoom: 15, bearing: 0, viewingAngle: 0)
            locationManager.stopUpdatingLocation()
        }
    }
}

